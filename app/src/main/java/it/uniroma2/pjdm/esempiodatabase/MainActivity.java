package it.uniroma2.pjdm.esempiodatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private GroceriesDBHelper dbHelper;
    private SQLiteDatabase db;
    private List<String> groceries;
    private ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new GroceriesDBHelper(this);
        db = dbHelper.getWritableDatabase();

        /*retrieve entries from the database*/
        updateGroceries();

        arrayAdapter =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1,
                        groceries);
        ListView listView = findViewById(R.id.listview1);
        listView.setAdapter(arrayAdapter);

    }

    private void updateGroceries() {
        String[] projection = {GroceriesContract.GroceryEntry.COLUMN_NAME_NAME};
        String sortOrder = GroceriesContract.GroceryEntry.COLUMN_NAME_NAME + " ASC";

        Cursor cursor = db.query(GroceriesContract.GroceryEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                sortOrder);

        List<String> newGroceries = new ArrayList<String>();
        while(cursor.moveToNext()) {
            int columnIndex =
                    cursor.getColumnIndex(GroceriesContract.GroceryEntry.COLUMN_NAME_NAME);
            String groceryName = cursor.getString(columnIndex);
            newGroceries.add(groceryName);
        }
        cursor.close();

        this.groceries = newGroceries;

        if(arrayAdapter != null) {
            arrayAdapter.clear();
            arrayAdapter.addAll(this.groceries);
        }
    }

    public void itemAdd(View view) {
        EditText editText = findViewById(R.id.edittext1);
        String groceryName = editText.getText().toString();
        editText.setText("");

        ContentValues values = new ContentValues();
        values.put(GroceriesContract.GroceryEntry.COLUMN_NAME_NAME, groceryName);
        values.put(GroceriesContract.GroceryEntry.COLUMN_NAME_TIME, System.currentTimeMillis());

        db.insert(GroceriesContract.GroceryEntry.TABLE_NAME, null, values);

        updateGroceries();
    }
}









