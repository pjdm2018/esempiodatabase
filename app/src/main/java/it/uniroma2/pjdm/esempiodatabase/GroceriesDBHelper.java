package it.uniroma2.pjdm.esempiodatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import it.uniroma2.pjdm.esempiodatabase.GroceriesContract.GroceryEntry;

/**
 * Created by clauz on 5/28/18.
 */

public class GroceriesDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "groceries.db";

    public GroceriesDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_ENTRIES =
                String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT, %s INTEGER)",
                        GroceryEntry.TABLE_NAME,
                        GroceryEntry._ID,
                        GroceryEntry.COLUMN_NAME_NAME,
                        GroceryEntry.COLUMN_NAME_TIME);

        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // TODO
    }
}
