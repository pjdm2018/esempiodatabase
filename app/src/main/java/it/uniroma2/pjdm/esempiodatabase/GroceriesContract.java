package it.uniroma2.pjdm.esempiodatabase;

import android.provider.BaseColumns;

/**
 * Created by clauz on 5/28/18.
 */

public class GroceriesContract {

    private GroceriesContract() {}

    public static class GroceryEntry implements BaseColumns {
        public static final String TABLE_NAME = "grocery";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_TIME = "timestamp";
    }
}
